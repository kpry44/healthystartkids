<?php get_header(); ?>



<div id="taxonomy-links_category" class="contentContainer">
   <h1><?php wp_title(); ?></h1>
   <div class="row">



      <div class="col-xs-9">
         <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
         <div class="item-taxonomy item-space">
            <h3><?php the_title(); ?></h3>
            <p><?php the_content(); ?></p>
            <?php $usefullinklink = get_post_meta( get_the_id(), 'usefullinks_link', true);
            if (!empty($usefullinklink)) { ?>
            <p><a href="<?php echo $usefullinklink; ?>" target="_blank"><button type="button" class="btn btn-primary"><?php _e('Go To Link ...', 'theme-healthy-start') ?></button></a></p>
            <?php } ?>
         </div>
         <?php endwhile; else: ?>
            <p><?php _e('Sorry, no links matched your criteria.'); ?></p>
         <?php endif; ?>
         <?php if( function_exists( 'wp_pagenavi' ) ) { wp_pagenavi(); } ?>
      </div>



      <div class="col-xs-3">
      <?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
         <div id="secondary" class="widget-area" role="complementary">
            <?php dynamic_sidebar( 'sidebar-3' ); ?>
         </div>
      <?php endif; ?>
      </div>



   </div>
</div>



<?php get_footer(); ?>