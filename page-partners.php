<?php /* Template Name: Partners */ ?>

<?php get_header(); ?>

<div id="page-partners" class="contentContainer">

   <?php get_template_part( 'snippets/page', 'content' ); ?>

   <?php // WP_Query arguments
   $args = array (
      'post_type'       => 'partner',
      'post_status'     => 'publish',
      );

      // The Query
   $query = new WP_Query( $args );

      // The Loop
   if ( $query->have_posts() ) {
      while ( $query->have_posts() ) {
         $query->the_post(); ?>
            <div class=" row item-partner">
               <div class="col-xs-4">
                  <?php the_post_thumbnail('medium', array( 'class' => 'img-responsive' )); ?>
               </div>
               <div class="col-xs-8">
                  <h3><?php the_title(); ?></h3>
                  <p><?php the_content(); ?></p>
                  <?php $partnerlink = get_post_meta( get_the_id(), 'partner_link', true);
                  if (!empty($partnerlink)) { ?>
                  <p><a href="<?php echo $partnerlink; ?>" target="_blank"><button type="button" class="btn btn-primary"><?php _e('Read more ...', 'theme-healthy-start'); ?></button></a></p>
                  <?php } ?>
               </div>
            </div>
      <?php }
   } else {
      echo _e('Sorry, there is nothing to display right now', 'theme-healthy-start');
   }

   if( function_exists( 'wp_pagenavi' ) ) { wp_pagenavi(); }

   // Restore original Post Data
   wp_reset_postdata(); ?>

</div><!-- archive-partners -->


<?php get_footer(); ?>