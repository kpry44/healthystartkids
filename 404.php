<?php get_header(); ?>

<div id="c404" class="contentContainer">
   <p class="c404error">404</p>
   <p class="lead">The page you are looking for can not be found or has moved.  Please use the navigation bar to explore the rest of the website.</p>
   <?php if ( is_active_sidebar( 'search' ) ) : ?>
   <div id="secondary" class="widget-area item-space" role="complementary">
      <?php dynamic_sidebar( 'search' ); ?>
   </div>
   <?php endif; ?>
</div>

<?php get_footer(); ?>