<?php get_header(); ?>



<div id="taxonomy-recipe_category" class="contentContainer">
   <h1><?php wp_title(); ?></h1>
   <div class="row">



      <div class="col-xs-9">
         <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
         <div class="item-taxonomy item-space">
            <div class="row">
                  <div class="col-sm-4">
                     <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('img-circle', array('class' => 'img-responsive floating-circle')); ?></a>
                  </div>
                  <div class="col-sm-8">
                     <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
                     <p><?php the_excerpt(); ?></p>
                  </div>
               </div>
         </div>
         <?php endwhile; else: ?>
            <p><?php _e('Sorry, no links matched your criteria.'); ?></p>
         <?php endif; ?>

         <!-- pagination -->
         <div class="row">
            <div class="col-xs-8 col-xs-offset-4">
               <?php if( function_exists( 'wp_pagenavi' ) ) { wp_pagenavi(); } ?>
            </div>
         </div>

      </div>



      <div class="col-xs-3">
      <?php if ( is_active_sidebar( 'sidebar-recipes' ) ) : ?>
         <div id="secondary" class="widget-area" role="complementary">
            <?php dynamic_sidebar( 'sidebar-recipes' ); ?>
         </div>
      <?php endif; ?>
      </div>



   </div>
</div>



<?php get_footer(); ?>