<?php /* Template Name: Playing With Healthy Start (Stories)*/ ?>

<?php get_header(); ?>

<div id="page-playing-with-healthy-start" class="contentContainer">

   <?php get_template_part( 'snippets/page', 'content' ); ?>

   <div class="well">
      <h4><?php _e('Submit A Story For Our Website', 'theme-healthy-start'); ?></h4>
      <a href="<?php _e('/submit-your-story/', 'theme-healthy-start'); ?>"><button type="button" class="btn btn-warning btn-lg"><?php _e('Submit', 'theme-healthy-start'); ?></button></a>
   </div>

   <div class="archive-stories">
      <?php // WP_Query arguments
      $args = array (
         'post_type'       => 'story',
         'post_status'     => 'publish',
         );

         // The Query
      $query = new WP_Query( $args );

         // The Loop
      if ( $query->have_posts() ) {
         while ( $query->have_posts() ) {
            $query->the_post(); ?>
               <div class="row item-playing-with-healthy-start">
                  <div class="col-xs-4">
                     <?php the_post_thumbnail('img-circle', array('class' => 'img-responsive floating-circle')); ?>
                  </div>
                  <div class="col-xs-8">
                     <h3><?php the_title(); ?></h3>
                     <p><?php the_content(); ?></p>
                     <p><a href="<?php the_permalink(); ?>"><button type="button" class="btn btn-primary"><?php _e('Read Full Story', 'theme-healthy-start') ?> ...</button></a></p>
                  </div>
               </div>
         <?php }
      } else {
         echo _e('Sorry, there is nothing to display right now', 'theme-healthy-start');
      } ?>

      <div class="row">
         <div class="col-xs-8 col-sm-offset-4">
            <?php if( function_exists( 'wp_pagenavi' ) ) { wp_pagenavi( array( 'query' =>$query ) ); } ?>
         </div>
      </div>

      <? // Restore original Post Data
      wp_reset_postdata(); ?>
   </div><!-- archive-stories -->
</div>


<?php get_footer(); ?>