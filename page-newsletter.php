<?php /* Template Name: Newsletters */ ?>

<?php get_header(); ?>

<div id="page-newsletter" class="contentContainer">

   <?php get_template_part( 'snippets/page', 'content' ); ?>

   <div class="row">
      <div class="col-xs-9">
      <?php // WP_Query arguments
      $args = array (
         'post_type'       => 'newsletter',
         'post_status'     => 'publish',
         'paged' => get_query_var('paged'),
         );

      // The Query
      $query = new WP_Query( $args );

      // The Loop
      if ( $query->have_posts() ) {
         while ( $query->have_posts() ) {
            $query->the_post(); ?>
               <div class="item-newsletter">
                  <?php the_post_thumbnail('medium', array('class' => 'img-responsive')); ?>
                  <h3><?php the_title(); ?></h3>
                  <p><?php the_content(); ?></p>
                  <?php $aaa = get_post_meta(get_the_ID(), 'newsletter_file_attachment', true);
                  if (!empty($aaa)) {
                     $bbb = $aaa['ID']; ?>
                     <p>
                        <a href="<?php echo wp_get_attachment_url($bbb); ?>" target="_blank"><button type="button" class="btn btn-primary"><?php _e( 'Download File', 'theme-healthy-start' ); ?></button></a>
                     </p>
                  <?php } ?>
                  </div>
                  <?php }
      } else {
         echo _e('Sorry, there is nothing to display right now', 'theme-healthy-start');
      }

      echo '<div class="clearfix"></div>';

      if( function_exists( 'wp_pagenavi' ) ) { wp_pagenavi( array( 'query' =>$query ) ); }

      // Restore original Post Data
      wp_reset_postdata(); ?>
      </div>

      <div class="col-xs-3">
         <?php if ( is_active_sidebar( 'sidebar-newsletter' ) ) : ?>
         <div id="secondary" class="widget-area" role="complementary">
            <?php dynamic_sidebar( 'sidebar-newsletter' ); ?>
         </div>
         <?php endif; ?>
      </div>

   </div>
</div><!-- archive-newsletter -->

<?php get_footer(); ?>