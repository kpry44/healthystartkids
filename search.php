<?php get_header(); ?>

<div id="search" class="contentContainer">

   <h1><?php wp_title(); ?></h1>

   <div class="row">
      <div class="col-xs-9">
         <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

         <div class="search-item">
            <h3><?php the_title(); ?></h3>
            <p class="post-date"><?php _e('Posted on', 'theme-healthy-start') ?>: <?php the_date(); ?></p>
            <p><?php the_excerpt(); ?></p>
            <p><a href="<?php the_permalink(); ?>"><button type="button" class="btn btn-primary"><?php _e('Read more ...', 'theme-healthy-start'); ?></button></a></p>
         </div>

         <?php endwhile; else: ?>
            <p><?php echo _e('Sorry, there is nothing to display right now', 'theme-healthy-start'); ?></p>
         <?php endif; ?>

      </div>

      <div class="col-xs-3">
         <?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
         <div id="secondary" class="widget-area" role="complementary">
            <?php dynamic_sidebar( 'sidebar-2' ); ?>
         </div>
      <?php endif; ?>
      </div>

   <?php if(function_exists(wp_pagenavi)){wp_pagenavi();} ?>
   </div>
</div>

<?php get_footer(); ?>