<?php /* Template Name: Recipes */ ?>

<?php get_header(); ?>

<div id="page-recipe" class="contentContainer">

   <?php get_template_part( 'snippets/page', 'content' ); ?>

   <div class="row">
      <div class="col-xs-9">
         <?php // WP_Query arguments
         $args = array (
            'post_type'       => 'recipe',
            'post_status'     => 'publish',
            'paged' => get_query_var('paged'),
            );

         // The Query
         $query = new WP_Query( $args );

         // The Loop
         if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
               $query->the_post(); ?>
               <div class="item-recipe item-space">
                  <div class="row">
                     <div class="col-sm-4">
                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('img-circle', array('class' => 'img-responsive floating-circle')); ?></a>
                     </div>
                     <div class="col-sm-8">
                        <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
                        <p><?php the_excerpt(); ?></p>
                     </div>
                  </div>
               </div>
                  <?php }
         } else {
            echo _e('Sorry, there is nothing to display right now', 'theme-healthy-start');
         }

         echo '<div class="clearfix"></div>'; ?>

         <!-- pagination -->
         <div class="row">
            <div class="col-xs-8 col-xs-offset-4">
               <?php if( function_exists( 'wp_pagenavi' ) ) { wp_pagenavi( array( 'query' =>$query ) ); } ?>
            </div>
         </div>

         <?php // Restore original Post Data
         wp_reset_postdata(); ?>

      </div>

      <div class="col-xs-3">
         <?php if ( is_active_sidebar( 'sidebar-recipes' ) ) : ?>
         <div id="secondary" class="widget-area" role="complementary">
            <?php dynamic_sidebar( 'sidebar-recipes' ); ?>
         </div>
         <?php endif; ?>
      </div>

   </div>
</div><!-- archive-recipes -->

<?php get_footer(); ?>