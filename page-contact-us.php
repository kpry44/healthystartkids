<?php /* Template Name: Contact Us */ ?>

<?php get_header(); ?>

<div id="page-contact-us" class="contentContainer">

   <?php get_template_part( 'snippets/page', 'content' ); ?>

   <div class="contact-form">
      <?php gravity_form(1, false, false, false, '', false); ?>
   </div>

</div><!-- contact-us -->

<?php get_footer(); ?>