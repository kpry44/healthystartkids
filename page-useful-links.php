<?php /* Template Name: Useful Links */ ?>

<?php get_header(); ?>

<div id="page-useful-links" class="contentContainer">

   <?php get_template_part( 'snippets/page', 'content' ); ?>

   <div class="row">
      <div class="col-xs-9">
         <?php // WP_Query arguments
         $args = array (
            'post_type'       => 'link',
            'post_status'     => 'publish',
            'paged' => get_query_var('paged'),
            );

            // The Query
         $query = new WP_Query( $args );

            // The Loop
         if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
               $query->the_post(); ?>
                  <div class="item-useful-link">
                     <h3><?php the_title(); ?></h3>
                     <p><?php the_content(); ?></p>
                     <?php $usefullinklink = get_post_meta( get_the_id(), 'usefullinks_link', true);
                     if (!empty($usefullinklink)) { ?>
                     <p><a href="<?php echo $usefullinklink; ?>" target="_blank"><button type="button" class="btn btn-primary"><?php _e('Go To Link ...', 'theme-healthy-start') ?></button></a></p>
                  </div>
                  <?php } ?>
            <?php }
         } else {
            echo _e('Sorry, there is nothing to display right now', 'theme-healthy-start');
         }

         if( function_exists( 'wp_pagenavi' ) ) { wp_pagenavi( array( 'query' =>$query ) ); }

         // Restore original Post Data
         wp_reset_postdata(); ?>
      </div>
      <div class="col-xs-3">
      <?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
         <div id="secondary" class="widget-area" role="complementary">
      <?php dynamic_sidebar( 'sidebar-3' ); ?>
         </div>
      <?php endif; ?>
      </div>
   </div>
</div><!-- archive-useful-links -->


<?php get_footer(); ?>