<?php get_header(); ?>



<div id="taxonomy-video_category" class="contentContainer">
   <h1><?php wp_title(); ?></h1>
   <div class="row">



      <div class="col-xs-9">
         <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
         $youtube_url = get_post_meta( $post->ID, 'videos_youtube_embed_code', true ); ?>
         <div class="item-taxonomy item-space">
            <div class="item-video">
               <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="<?php echo $youtube_url; ?>"></iframe>
               </div>
             <h3><?php the_title(); ?></h3>
             <?php the_content(); ?>
            </div>
         </div>
         <?php endwhile; else: ?>
            <p><?php _e('Sorry, no links matched your criteria.'); ?></p>
         <?php endif; ?>
         <?php if( function_exists( 'wp_pagenavi' ) ) { wp_pagenavi(); } ?>
      </div>



      <div class="col-xs-3">
      <?php if ( is_active_sidebar( 'sidebar-video' ) ) : ?>
         <div id="secondary" class="widget-area" role="complementary">
            <?php dynamic_sidebar( 'sidebar-video' ); ?>
         </div>
      <?php endif; ?>
      </div>



   </div>
</div>



<?php get_footer(); ?>