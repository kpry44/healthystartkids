<?php get_header(); ?>

<div id="page" class="contentContainer">

   <?php get_template_part( 'snippets/page', 'content' ); ?>

</div>

<?php get_footer(); ?>