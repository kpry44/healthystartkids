<!DOCTYPE html>
<html lang="<?php bloginfo('language'); ?>">
<head>
   <meta charset="<?php bloginfo('charset'); ?>">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/image/favicon.ico" type="image/x-icon" />
   <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/image/favicon.ico" type="image/x-icon" />
   <?php wp_head(); ?>
   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
   <![endif]-->
</head>

<body>

   <div class="header">
      <div class="container">
         <div class="row">

            <div class="col-xs-3">
               <img src="<?php bloginfo('stylesheet_directory'); ?>/image/dreamstime.png" alt="Healthy Start Saskatchewan" id="child">
            </div>

            <div class="col-xs-5">
               <a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/image/logo.png" alt="Healthy Start Saskatchewan Logo" id="logo"></a>
            </div>

            <div class="col-xs-4">
               <a href="<?php _e('/newsletter-sign-up/', 'theme-healthy-start') ?>" class="newsletter-action btn btn-primary">
                  <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><?php _e('Newsletter Sign Up', 'theme-healthy-start') ?>
               </a>

             <?php if ( is_active_sidebar('sidebar-1') ) {
                  dynamic_sidebar( 'sidebar-1' );
               } ?>
            </div>

         </div>
      </div>
   </div>


   <div class="navigation">
      <nav class="navbar" role="navigation">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="container">
            <div class="navbar-header">
            </div>
            <?php
            wp_nav_menu( array(
               'menu'              => 'primary',
               'theme_location'    => 'primary',
               'depth'             => 2,
               'container'         => 'div',
               'container_class'   => 'nav-container pull-right',
               'menu_class'        => 'nav navbar-nav',
               'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
               'walker'            => new wp_bootstrap_navwalker())
            );
            ?>
         </div>
      </nav>
   </div>

   <div class="container">