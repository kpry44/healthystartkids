<?php get_header(); ?>

<div id="single" class="contentContainer">

<?php while ( have_posts() ) : the_post(); ?>

   <div class="intro">
      <h1><?php the_title(); ?></h1>
   </div>

   <div class="row">
      <div class="col-xs-8">
         <p><?php the_content(); ?></p>
      </div>
      <div class="col-xs-4">
         <?php the_post_thumbnail('img-circle', array( 'class' => 'img-responsive floating-circle')); ?>
      </div>
   </div>

<?php endwhile; ?>

</div>

<?php get_footer(); ?>