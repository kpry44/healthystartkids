<?php /* Template Name: Videos */ ?>

<?php get_header(); ?>

<div id="page-video" class="contentContainer">

   <?php get_template_part( 'snippets/page', 'content' ); ?>

   <div class="row">
      <div class="col-xs-9">
      <?php // WP_Query arguments
      $args = array (
         'post_type'       => 'video',
         'post_status'     => 'publish',
         'paged' => get_query_var('paged'),
         );

      // The Query
      $query = new WP_Query( $args );

      // The Loop
      if ( $query->have_posts() ) {
         while ( $query->have_posts() ) {
            $query->the_post();
               $youtube_url = get_post_meta( $post->ID, 'videos_youtube_embed_code', true ); ?>
            <div class="item-video">
               <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="<?php echo $youtube_url; ?>"></iframe>
               </div>
             <h3><?php the_title(); ?></h3>
             <?php the_content(); ?>
            </div>
               <?php }
      } else {
         echo _e('Sorry, there is nothing to display right now', 'theme-healthy-start');
      }

      echo '<div class="clearfix"></div>';

      if( function_exists( 'wp_pagenavi' ) ) { wp_pagenavi( array( 'query' =>$query ) ); }

      // Restore original Post Data
      wp_reset_postdata(); ?>
      </div>

      <div class="col-xs-3">
         <?php if ( is_active_sidebar( 'sidebar-video' ) ) : ?>
         <div id="secondary" class="widget-area" role="complementary">
            <?php dynamic_sidebar( 'sidebar-video' ); ?>
         </div>
         <?php endif; ?>
      </div>

   </div>
</div><!-- archive-video -->

<?php get_footer(); ?>