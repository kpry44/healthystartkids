<?php /* Template Name: HS @ Home */ ?>

<?php get_header(); ?>

<div id="page-hs-home" class="contentContainer">

   <?php get_template_part( 'snippets/page', 'content' ); ?>

   <div class="row">
      <div class="col-xs-9">
         <?php // WP_Query arguments
         $args = array (
            'post_type'       => 'hs_home',
            'post_status'     => 'publish',
            'paged' => get_query_var('paged'),
            );

            // The Query
         $query = new WP_Query( $args );

            // The Loop
         if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
               $query->the_post(); ?>
                  <div class="item-useful-hs_home">
                     <h3><?php the_title(); ?></h3>
                     <p><?php the_content(); ?></p>
                  </div>
            <?php }
         } else {
            echo _e('Sorry, there is nothing to display right now', 'theme-healthy-start');
         }

         wp_pagenavi( array( 'query' =>$query ) );
         // Restore original Post Data
         wp_reset_postdata(); ?>
      </div>
      <div class="col-xs-3">
         <?php if ( is_active_sidebar( 'sidebar-hs_home' ) ) : ?>
      <div id="secondary" class="widget-area" role="complementary">
         <?php dynamic_sidebar( 'sidebar-hs_home' ); ?>
      </div>
   <?php endif; ?>
      </div>
   </div>
</div><!-- archive-hs-at-home -->


<?php get_footer(); ?>