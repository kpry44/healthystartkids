<?php /* Template Name: Research Members */ ?>

<?php get_header(); ?>

<div id="page-research-member" class="contentContainer">

   <?php get_template_part( 'snippets/page', 'content' ); ?>

   <h2><?php echo _e('Our Research Team', 'theme-healthy-start'); ?></h2>
   <?php // WP_Query arguments
   $args = array (
      'post_type'       => 'research_member',
      'post_status'     => 'publish',
      );

   // The Query
   $query = new WP_Query( $args );

   // The Loop
   if ( $query->have_posts() ) {
      while ( $query->have_posts() ) {
         $query->the_post(); ?>
               <div class="row item-research-member">
                  <div class="col-xs-4">
                     <?php the_post_thumbnail('img-circle', array('class' => 'img-responsive floating-circle')); ?>
                  </div>
                  <div class="col-xs-8">
                     <h3><?php the_title(); ?></h3>
                     <p class="lead"><?php echo get_post_meta( get_the_id(), 'research_role', true); ?> - <?php echo get_post_meta( get_the_id(), 'research_info', true); ?></p>
                     <p><?php the_content(); ?></p>
                  </div>
               </div>
         <?php }
   } else {
      echo _e('Sorry, there is nothing to display right now', 'theme-healthy-start');
   } ?>

   <!-- pagination -->
   <div class="row">
      <div class="col-xs-8 col-xs-offset-4">
         <?php if( function_exists( 'wp_pagenavi' ) ) { wp_pagenavi( array( 'query' =>$query ) ); } ?>
      </div>
   </div>

   <?php // Restore original Post Data
   wp_reset_postdata(); ?>
</div><!-- archive-research_members -->

<?php get_footer(); ?>