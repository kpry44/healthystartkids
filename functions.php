<?php

// ENQUEUE JAVASCRIPTS CSS STYLES
function lime_enqueue_scripts() {
   wp_enqueue_style( 'bootstrap-css', get_stylesheet_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css' );
   wp_enqueue_style( 'styles', get_stylesheet_uri() );
   if(ICL_LANGUAGE_CODE=='en') {
      wp_enqueue_style( 'quotes-english', get_template_directory_uri() . '/snippets/quotes-english.css');
   }
   if(ICL_LANGUAGE_CODE=='fr') {
      wp_enqueue_style( 'quotes-french', get_template_directory_uri() . '/snippets/quotes-french.css');
   }
   wp_enqueue_script( 'bootstrap-js', get_stylesheet_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js', array('jquery'), '', true);
   wp_enqueue_script( 'bootstrap_gf_js', get_stylesheet_directory_uri().'/assets/bootstrap-gravity-forms/bgf.js', array('jquery'), '', true );
}
add_action('wp_enqueue_scripts', 'lime_enqueue_scripts' );


if ( ! function_exists( 'healthystart_setup' ) ) :
   function healthystart_setup() {

      // Add default posts and comments RSS feed links to head.
      add_theme_support( 'automatic-feed-links' );

      /*
       * Let WordPress manage the document title.
       * By adding theme support, we declare that this theme does not use a
       * hard-coded <title> tag in the document head, and expect WordPress to
       * provide it for us.
       */
      add_theme_support( 'title-tag' );

      //Enable support for Post Thumbnails on posts and pages.
      add_theme_support( 'post-thumbnails' );

      // ADD IMAGE SIZE: PERFECT SQUARE
      add_image_size( 'img-circle', 500, 500, true );

      // REGISTER NAVIGATION MENUS
      register_nav_menus( array(
         'primary' => __( 'Primary Menu', 'healthystartkids' ),
      ) );

   }
endif;
add_action( 'after_setup_theme', 'healthystart_setup' );



//REGISTER WIDGET AREAS
function healthy_start_widgets_init() {
// LANGUAGE SWITCHER WIDGET
   register_sidebar( array(
      'name' => __( 'Language Selector Area', 'wpb' ),
      'id' => 'sidebar-1',
      'description' => __( 'The language selector appears in the right side of the header', 'wpb' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h5 class="widget-title">',
      'after_title' => '</h5>',
      ) );
// BLOG SIDEBAR WIDGET
   register_sidebar( array(
      'name' => __( 'Blog Sidebar', 'wpb' ),
      'id' => 'sidebar-2',
      'description' => __( 'The sidebar for blog taxonomy menus', 'wpb' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
      ) );
   register_sidebar( array(
      'name' => __( 'Useful Links Sidebar', 'wpb' ),
      'id' => 'sidebar-3',
      'description' => __( 'The sidebar for useful links', 'wpb' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
      ) );
   register_sidebar( array(
      'name' => __( 'Recipes Sidebar', 'wpb' ),
      'id' => 'sidebar-recipes',
      'description' => __( 'The sidebar for recipes', 'wpb' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
      ) );
   register_sidebar( array(
      'name' => __( 'HS @ Home Sidebar', 'wpb' ),
      'id' => 'sidebar-hs_home',
      'description' => __( 'The sidebar for hs @ home', 'wpb' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
      ) );
   register_sidebar( array(
      'name' => __( 'Videos Sidebar', 'wpb' ),
      'id' => 'sidebar-video',
      'description' => __( 'The sidebar for videos', 'wpb' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
      ) );
   register_sidebar( array(
      'name' => __( 'Newsletters Sidebar', 'wpb' ),
      'id' => 'sidebar-newsletter',
      'description' => __( 'The sidebar for newsletters', 'wpb' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
      ) );
   register_sidebar( array(
      'name' => __( 'Progress Reports Sidebar', 'wpb' ),
      'id' => 'sidebar-progress_reports',
      'description' => __( 'The sidebar for progress reports', 'wpb' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
      ) );
   register_sidebar( array(
      'name' => __( 'Mailchimp Widget', 'wpb' ),
      'id' => 'sidebar-4',
      'description' => __( 'The sidebar for mailchimp widget in footer', 'wpb' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
      ) );
   register_sidebar( array(
      'name' => __( 'Contact Us Widget', 'wpb' ),
      'id' => 'sidebar-5',
      'description' => __( 'The sidebar for contact us widget in footer', 'wpb' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
      ) );
   register_sidebar( array(
      'name' => __( 'Search Form Widget', 'wpb' ),
      'id' => 'search-form-widget',
      'description' => __( 'Search Form Widget', 'wpb' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
      ) );
   register_sidebar( array(
      'name' => __( 'Search', 'wpb' ),
      'id' => 'search',
      'description' => __( 'The sidebar for 404.php', 'wpb' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
      ) );
}
add_action( 'widgets_init', 'healthy_start_widgets_init' );



// REGISTER CUSTOM NAVIGATION WALKER
require_once('wp_bootstrap_navwalker.php');



// CHANGE EXCERPT LENGTH
function custom_excerpt_length( $length ) {
   return 25;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );



// REGISTER & ENQUEUE JQUERY FROM GOOGLE CDN
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js", false, null);
   wp_enqueue_script('jquery');
}



// WP-PAGENAVI
//attach our function to the wp_pagenavi filter
add_filter( 'wp_pagenavi', 'ik_pagination', 10, 2 );
//customize the PageNavi HTML before it is output
function ik_pagination($html) {
   $out = '';
//wrap a's and span's in li's
   $out = str_replace("<div","",$html);
   $out = str_replace("class='wp-pagenavi'>","",$out);
   $out = str_replace("<a","<li><a",$out);
   $out = str_replace("</a>","</a></li>",$out);
   $out = str_replace("<span","<li><span",$out);
   $out = str_replace("</span>","</span></li>",$out);
   $out = str_replace("</div>","",$out);
   return '<ul class="pagination pagination-centered">'.$out.'</ul>';
}