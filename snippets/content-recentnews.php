<div class="recent-news-widget">
   <div class="row">
      <div class="col-xs-3"></div>
      <div class="col-xs-9">
         <h3 class="widget-title"><?php _e('Recent News','theme-text-domain') ?></h3>
      </div>
   </div>

<?php // WP_Query arguments
$args = array (
   'post_type'              => 'post',
   'post_status'            => 'publish',
   'posts_per_page'         => '2',
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
   while ( $query->have_posts() ) {
      $query->the_post(); ?>
      <div class="col-xs-3">
         <?php the_post_thumbnail('img-circle', array( 'class' => 'img-responsive')); ?>
      </div>
      <div class="col-xs-9">
         <h5><?php the_title(); ?></h5>
         <?php the_excerpt(); ?>
         <a href="<?php the_permalink(); ?>"><button type="button" class="btn btn-danger"><?php _e('Read more ...', 'theme-healthy-start'); ?></button></a>
      </div>
   <?php }
} else {
   echo _e('Sorry, there is nothing to display right now', 'theme-healthy-start');
}

// Restore original Post Data
wp_reset_postdata(); ?>

</div><!-- recent-news-widget -->

