<div class="page-content">

   <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

         <h1><?php wp_title(); ?></h1>
         <?php the_content(); ?>

   <?php endwhile; else: endif; ?>

</div><!-- page-content -->