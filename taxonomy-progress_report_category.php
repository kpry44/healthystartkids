<?php get_header(); ?>



<div id="taxonomy-progress_report_category" class="contentContainer">
   <h1><?php wp_title(); ?></h1>
   <div class="row">



      <div class="col-xs-9">
         <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
         <div class="item-taxonomy item-space">
            <h3><?php the_title(); ?></h3>
            <p><?php the_content(); ?></p>
               <?php $aaa = get_post_meta(get_the_ID(), 'progress_report_file_attachment', true);
               if (!empty($aaa)) {
                  $bbb = $aaa['ID']; ?>
                  <p>
                     <a href="<?php echo wp_get_attachment_url($bbb); ?>" target="_blank"><button type="button" class="btn btn-primary">Download File</button></a>
                  </p>
               <?php } ?>
         </div>
         <?php endwhile; else: ?>
            <p><?php _e('Sorry, no links matched your criteria.'); ?></p>
         <?php endif; ?>
         <?php if( function_exists( 'wp_pagenavi' ) ) { wp_pagenavi(); } ?>
      </div>



      <div class="col-xs-3">
      <?php if ( is_active_sidebar( 'sidebar-progress_reports' ) ) : ?>
         <div id="secondary" class="widget-area" role="complementary">
            <?php dynamic_sidebar( 'sidebar-progress_reports' ); ?>
         </div>
      <?php endif; ?>
      </div>



   </div>
</div>



<?php get_footer(); ?>