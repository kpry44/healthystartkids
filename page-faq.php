<?php /* Template Name: FAQ */ ?>

<?php get_header(); ?>

<div id="page-faq" class="contentContainer">

   <?php get_template_part( 'snippets/page', 'content' ); ?>

   <div class="panel-group" id="accordion">
   <?php // WP_Query arguments
   $args = array (
      'post_type'       => 'faq',
      'post_status'     => 'publish',
      );

   // The Query
   $query = new WP_Query( $args );
   $cnt = 1;

   // The Loop
   if ( $query->have_posts() ) {
      while ( $query->have_posts() ) {
         $query->the_post(); ?>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#<?php the_id(); ?>">
                        <?php the_title(); ?>
                     </a>
                  </h4>
               </div>
               <div id="<?php the_id(); ?>" class="panel-collapse collapse collapse <?php if($cnt==1){echo 'in'; } ?>">
                  <div class="panel-body">
                     <?php the_content(); ?>
                  </div>
               </div>
            </div>
      <?php
      $cnt++; }
      } else {
         echo _e('Sorry, there is nothing to display right now', 'theme-healthy-start');
      }

      // Restore original Post Data
      wp_reset_postdata(); ?>

   </div>
</div><!-- archive-faq -->


<?php get_footer(); ?>