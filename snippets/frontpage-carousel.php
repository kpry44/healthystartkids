<div id="healthy-carousel" class="carousel slide" data-ride="carousel">

   <!-- Wrapper for slides -->
   <div class="carousel-inner">

   <?php
   $cnt = 1;

   // WP_Query arguments
   $args = array (
      'post_type'              => 'slide',
      'post_status'            => 'publish',
   );

   // The Query
   $query = new WP_Query( $args );

   // The Loop
   if ( $query->have_posts() ) { ?>
      <?php while ( $query->have_posts() ) {
         $query->the_post(); ?>

         <?php
            $link_to = get_post_meta( $post->ID, 'link_to', true );
            $bg_colour = get_post_meta( $post->ID, 'slides_bg_colour', true );
         ?>

         <div class="item <?php if($cnt==1){echo'active';} ?>">
            <div class="row">
               <div class="col-xs-4">
                  <div class="content" style="background: <?php if( empty( $bg_colour ) ) { echo '#DE352E'; } else { echo $bg_colour; } ?> url('<?php echo get_stylesheet_directory_uri(); ?>/image/png.png'); ">
                     <h4><?php the_title(); ?></h4>
                     <?php if( !empty($link_to) ) { ?>
                     <a href="<?php echo $link_to; ?>" class="btn btn-primary"><?php _e('Read more ...', 'theme-healthy-start') ?></a>
                     <?php } ?>
                  </div>
               </div>
               <div class="col-xs-8">
                  <?php the_post_thumbnail(); ?>
               </div>
            </div>
         </div>

      <?php $cnt++;
      }
   } else {
      echo '';
   }

   // Restore original Post Data
   wp_reset_postdata(); ?>

   </div>

   <!-- Controls -->
   <a class="left carousel-control" href="#healthy-carousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
   </a>
   <a class="right carousel-control" href="#healthy-carousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
   </a>
</div>