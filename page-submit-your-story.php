<?php /*Template Name: Submit Your Story*/ ?>

<?php get_header(); ?>

<div id="page-submit-your-story" class="contentContainer">

   <?php get_template_part( 'snippets/page', 'content' ); ?>

   <div class="contact-form">
      <?php gravity_form(2, false, false, false, '', false); ?>
   </div>

</div><!-- submit-your-story -->

<?php get_footer(); ?>