<?php get_header(); ?>

<div id="home" class="contentContainer">

   <div class="intro">
      <h1>» <?php _e('News', 'theme-healthy-start') ?></h1>
   </div>

   <div class="row">
      <div class="col-xs-9">
         <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
         <div class="row item-home">
            <div class="col-xs-4">
               <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('img-circle', array( 'class' => 'img-responsive floating-circle')); ?></a>
            </div>
            <div class="col-xs-8">
               <h3><?php the_title(); ?></h3>
               <p class="post-date"><?php _e('Posted on', 'theme-healthy-start') ?>: <?php the_date(); ?></p>
               <p><?php the_excerpt(); ?></p>
               <p><a href="<?php the_permalink(); ?>"><button type="button" class="btn btn-primary"><?php _e('Read more ...', 'theme-healthy-start'); ?></button></a></p>
            </div>
         </div>
      <?php endwhile; else: ?>
         <p><?php echo _e('Sorry, there is nothing to display right now', 'theme-healthy-start'); ?></p>
      <?php endif; ?>

         <div class="row">
            <div class="col-sm-8 col-sm-offset-4">
               <?php if( function_exists( 'wp_pagenavi' ) ) { wp_pagenavi(); } ?>
            </div>
         </div>

      </div>
      <div class="col-xs-3">
         <?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
         <div id="secondary" class="widget-area" role="complementary">
            <?php dynamic_sidebar( 'sidebar-2' ); ?>
         </div>
         <?php endif; ?>
      </div>
   </div>

</div>


<?php get_footer(); ?>