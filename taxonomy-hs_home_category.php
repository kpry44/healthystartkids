<?php get_header(); ?>



<div id="taxonomy-hs_home_category" class="contentContainer">
   <h1><?php wp_title(); ?></h1>
   <div class="row">



      <div class="col-xs-9">
         <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
         <div class="item-taxonomy item-space">
            <h3><?php the_title(); ?></h3>
            <p><?php the_content(); ?></p>
         </div>
         <?php endwhile; else: ?>
            <p><?php _e('Sorry, no links matched your criteria.'); ?></p>
         <?php endif; ?>
         <?php if( function_exists( 'wp_pagenavi' ) ) { wp_pagenavi(); } ?>
      </div>



      <div class="col-xs-3">
      <?php if ( is_active_sidebar( 'sidebar-hs_home' ) ) : ?>
         <div id="secondary" class="widget-area" role="complementary">
            <?php dynamic_sidebar( 'sidebar-hs_home' ); ?>
         </div>
      <?php endif; ?>
      </div>



   </div>
</div>



<?php get_footer(); ?>