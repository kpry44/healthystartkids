<?php get_header(); ?>

<div id="single-resource" class="contentContainer">

   <h1><?php wp_title(); ?></h1>

   <?php while ( have_posts() ) : the_post(); ?>

   <div class="row">
      <div class="col-xs-8">
         <p><?php the_content(); ?></p>
         <?php $resourcelink = get_post_meta( get_the_id(), 'partner_link', true);
         if (!empty($resourcelink)) { ?>
         <p class="link"><?php _e('For more information, visit their website', 'theme-healthy-start') ?> :
            <a href="<?php echo $resourcelink; ?>" target="_blank"><button type="button" class="btn btn-primary btn-lg"><?php _e('View Website', 'theme-healthy-start') ?></button></a>
         </p>
         <?php } ?>
      </div>
      <div class="col-xs-4">
         <?php the_post_thumbnail('img-circle', array( 'class' => 'img-responsive floating-circle')); ?>
      </div>
   </div>

<?php endwhile; ?>

</div><!-- /single-resource -->

<?php get_footer(); ?>