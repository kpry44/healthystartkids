<?php /* Template Name: Team Members */ ?>

<?php get_header(); ?>

<div id="page-our-team" class="contentContainer">

   <?php get_template_part( 'snippets/page', 'content' ); ?>

   <?php // WP_Query arguments
   $args = array (
      'post_type'       => 'team_member',
      'post_status'     => 'publish',
      );

   // The Query
   $query = new WP_Query( $args );

   // The Loop
   if ( $query->have_posts() ) {
      while ( $query->have_posts() ) {
         $query->the_post();
            $teammember_position = get_post_meta( $post->ID, 'teammember_position', true );
            $teammember_location = get_post_meta( $post->ID, 'teammember_location', true );
            $teammember_email = get_post_meta( $post->ID, 'teammember_email', true );
            $teammember_phone = get_post_meta( $post->ID, 'teammember_phone', true ); ?>
         <div class="row item-our-team">
            <div class="col-xs-4">
               <?php the_post_thumbnail('img-circle', array('class' => 'img-responsive floating-circle')); ?>
            </div>
            <div class="col-xs-8">
               <h3><?php the_title(); ?></h3>
               <?php if( !empty( $teammember_position) || !empty( $teammember_location) ) { ?>
                  <p class="lead"><?php echo $teammember_position; ?> - <?php echo $teammember_location; ?></p>
               <?php } ?>
                  <p><?php the_content(); ?></p>
               <?php if( !empty( $teammember_email ) ) { ?>
                  <div class="teammember_email well">
                     <span class="glyphicon glyphicon-send" aria-hidden="true"></span> <?php echo $teammember_email; ?>
                  </div>
               <?php } ?>
               <?php if( !empty( $teammember_phone ) ) { ?>
                  <div>
                     <div class="teammember_phone well"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> <?php echo $teammember_phone; ?></div>
                  </div>
               <?php } ?>

            </div>
         </div>
      <?php }
   } else {
      echo _e('Sorry, there is nothing to display right now', 'theme-healthy-start');
   } ?>

   <div class="row">
      <div class="col-xs-8 col-xs-offset-4">
         <?php if( function_exists( 'wp_pagenavi' ) ) { wp_pagenavi(); } ?>
      </div>
   </div>

   <?php // Restore original Post Data
   wp_reset_postdata(); ?>
</div><!-- archive-team_members -->


<?php get_footer(); ?>