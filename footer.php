</div><!-- /container  -->

<div id="testimonials">
   <div class="container">
      <?php // WP_Query arguments
      $args = array (
         'post_type'       => 'testimonial',
         'post_status'     => 'publish',
         'posts_per_page'  => '1',
         'orderby'         => 'rand'
         );

      // The Query
      $query = new WP_Query( $args );

      // The Loop
      if ( $query->have_posts() ) {
         while ( $query->have_posts() ) {
            $query->the_post(); ?>
            <blockquote><?php the_content(); ?></blockquote>
            - <?php the_title(); ?>
         <?php }
      } else {
         echo 'Sorry - there are no testimonials to display right now';
      }

      // Restore original Post Data
         wp_reset_postdata(); ?>
   </div>
</div>


<!-- BEGIN FOOTER -->
<div id="footer">
   <div class="container">
      <div class="row widget-area">
         <div class="col-xs-6">
            <?php get_template_part( 'snippets/content', 'recentnews' ); ?>
         </div>
         <div class="col-xs-3">
            <?php if ( is_active_sidebar( 'sidebar-4' ) ) : ?>
            <div id="first" class="widget-area" role="complementary">
               <?php dynamic_sidebar( 'sidebar-4' ); ?>
            </div>
            <?php endif; ?>
         </div>
         <div class="col-xs-3">
            <?php if ( is_active_sidebar( 'sidebar-5' ) ) : ?>
            <div id="secondary" class="widget-area" role="complementary">
               <?php dynamic_sidebar( 'sidebar-5' ); ?>
            </div>
            <?php endif; ?>
            <div class="social">
               <?php if(ICL_LANGUAGE_CODE=='en') { ?>
               <a href="http://twitter.com/hsds_ca" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/image/twitter.png" alt="Twitter"></a>
               <?php } ?>
               <?php if(ICL_LANGUAGE_CODE=='fr') { ?>
               <a href="//twitter.com/DepartSante" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/image/twitter.png" alt="Twitter"></a>
               <?php } ?>
               <a href="//facebook.com/hsds.ca" target="_blank"><img src="<?php bloginfo('stylesheet_directory'); ?>/image/facebook.png" alt="Facebook"></a>
            </div>
         </div>
      </div>
      <div class="partnership">
         <?php _e('An Innovation Strategy Phase II Project: "Achieving Healthier Weights in Canada\'s Communities"','theme-text-domain'); ?>
         <div class="row">
            <div class="col-xs-6">
            <?php _e('Funded by', 'theme-healthy-start') ?>:<br>
               <a href="//www.phac-aspc.gc.ca/index-eng.php"><img src="<?php bloginfo('stylesheet_directory'); ?>/image/partnership-2.png" title="Public Health Agency of Canada" alt="Public Health Agency of Canada"></a>
            </div>
            <div class="col-xs-6">
            <?php _e('Led by', 'theme-healthy-start') ?>:<br>
            <a href="//www.rsfs.ca/"><img src="<?php bloginfo('stylesheet_directory'); ?>/image/partnership-1.png" title="Reseau Santé en français de la Saskatchewan" alt="Reseau Santé en français de la Saskatchewan"></a>
            </div>
         </div><!-- row -->
      </div>
   </div>
</div><!-- footer -->


<?php wp_footer(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52198608-1', 'healthystartkids.ca');
  ga('send', 'pageview');

</script>
</body>
</html>