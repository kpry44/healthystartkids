<?php /*Template Name: Resources*/ ?>

<?php get_header(); ?>

<div id="page-resources" class="contentContainer">

   <?php get_template_part( 'snippets/page', 'content' ); ?>

   <div class="row">
      <div class="col-xs-6 item-resource">
         <?php if ( ICL_LANGUAGE_CODE=='en' ) { ?>
            <img src="<?php bloginfo('stylesheet_directory'); ?>/image/newsletters-en.jpg" alt="" class="img-responsive floating-circle">
         <?php }
         elseif ( ICL_LANGUAGE_CODE=='fr' ) { ?>
            <img src="<?php bloginfo('stylesheet_directory'); ?>/image/newsletters-fr.jpg" alt="" class="img-responsive floating-circle">
         <?php }
         else {
         } ?>
         <h3><?php _e('Newsletters', 'theme-healthy-start'); ?></h3>
         <p><?php _e('The Healthy Start Sampler is a monthly newsletter for educators and parents.  Each issue has information on physical activity, healthy eating, a tasty recipe, and more.  Find all past issues here.', 'theme-healthy-start'); ?></p>
         <p><a href="<?php _e('/newsletters/', 'theme-healthy-start'); ?>"><button type="button" class="btn btn-primary"><?php _e('Read more ...', 'theme-healthy-start'); ?></button></a></p>
      </div>
      <div class="col-xs-6 item-resource">
         <?php if ( ICL_LANGUAGE_CODE=='en' ) { ?>
            <img src="<?php bloginfo('stylesheet_directory'); ?>/image/usefullinks-en.jpg" alt="" class="img-responsive floating-circle">
         <?php }
         elseif ( ICL_LANGUAGE_CODE=='fr' ) { ?>
            <img src="<?php bloginfo('stylesheet_directory'); ?>/image/usefullinks-fr.jpg" alt="" class="img-responsive floating-circle">
         <?php }
         else {
         } ?>
         <h3><?php _e('Useful Links', 'theme-healthy-start'); ?></h3>
         <p><?php _e('The Internet is full of useful information, but there is so much there that it can feel overwhelming.  As we come across reputable useful, and interesting sites, we will post them here.  Browse our links for recipes, easy game ideas, articles, and more!', 'theme-healthy-start'); ?></p>
         <p><a href="<?php _e('/useful-links/', 'theme-healthy-start'); ?>"><button type="button" class="btn btn-primary"><?php _e('Read more ...', 'theme-healthy-start'); ?></button></a></p>
      </div>
   </div>

   <div class="row">
      <?php

      $cnt = 1;

      // WP_Query arguments
      $args = array (
         'post_type'       => 'resource',
         'post_status'     => 'publish',
         );

         // The Query
      $query = new WP_Query( $args );

         // The Loop
      if ( $query->have_posts() ) {
         while ( $query->have_posts() ) {
            $query->the_post(); ?>
               <div class="col-xs-6 item-resource">
               <?php the_post_thumbnail('medium', array('class' => 'img-responsive floating-circle')); ?>
               <h3><?php the_title(); ?></h3>
               <p><?php the_excerpt(); ?></p>
               <p><a href="<?php the_permalink(); ?>"><button type="button" class="btn btn-primary"><?php _e('Read more ...', 'theme-healthy-start'); ?></button></a></p>
            </div>
            <?php if( $cnt%2 == 0 ) { echo '</div><div class="row">'; } ?>
         <?php $cnt++; }
      } else {
         echo _e('Sorry, there is nothing to display right now', 'theme-healthy-start');
      }

      // Restore original Post Data
      wp_reset_postdata(); ?>
   </div>
</div><!-- archive-resources -->


<?php get_footer(); ?>