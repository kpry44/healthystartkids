<?php get_header(); ?>

<div id="front-page" class="contentContainer">

   <div class="welcome item-space">
   <?php get_template_part( 'snippets/frontpage', 'carousel' );
   if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <span class="intro-text
      "><?php the_content(); ?></span>
   <?php endwhile; else: ?>
      <p>Sorry, no posts to list</p>
   <?php endif; ?>
   </div>

   <div class="recent-news">
      <div class="container">
         <div class="row">
            <?php // WP_Query arguments
            $args = array (
               'post_status'            => 'publish',
               'posts_per_page'         => '3',
            );
            // The Query
            $query = new WP_Query( $args );
            // The Loop
            if ( $query->have_posts() ) {
               while ( $query->have_posts() ) {
                  $query->the_post(); ?>
                                 <div class="col-xs-4">
                                    <?php the_post_thumbnail( 'img-circle', array( 'class' => 'img-responsive floating-circle' ) ); ?>
                                    <h3><?php the_title(); ?></h3>
                                    <?php the_excerpt(); ?>
                                    <a href="<?php the_permalink(); ?>"><button type="button" class="btn btn-primary"><?php _e( 'Read more ...', 'theme-healthy-start' ); ?></button></a>
                                 </div>
                              <?php }
            } else {
               'no posts found';
            }
            // Restore original Post Data
            wp_reset_postdata(); ?>
         </div>
      </div>
   </div>

</div><!-- /front-page -->

<?php get_footer();
